<!DOCTYPE html>



<html>
	<head>

		<script type="text/javascript" src="jquery-1.11.2.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
		<script type="text/javascript" src="addHtml.js" ></script>
		<script type="text/javascript" src="ajaxFileUpload.js"></script>
		<script type="text/javascript">


		//TODO: Assign to own file
		function postMultiple(){


			/* Configuration */
			var formId = 'form';
			var inputId = 'userfile';
			var postURL = 'processHandler/uploadImages';

			/* Document Elements */
			var form = document.getElementById(formId);
			var fileSelect = document.getElementById(inputId);
		
			var files = fileSelect.files;

			var formData = new FormData();

			// Loop through each of the selected files.
			for (var i = 0; i < files.length; i++) {
			  var file = files[i];

			  console.log('File: ' + file);
			  // Check the file type.
			  if (!file.type.match('image.*')) {
			    continue;
			  }

			  // Add the file to the request.
			  formData.append('photos[]', file, file.name);
			}


			// Set up the request.
			var xhr = new XMLHttpRequest();
			xhr.open('POST', postURL, true);

			// Set up a handler for when the request finishes.
			xhr.onload = function () {
			  if (xhr.status === 200) {			    

			  	console.log("Image Upload: Success");
			  	fileNames = xhr.responseText;
			  	console.log(xhr.responseText);

			  } else {
			    alert('An error occurred. Check Console Log');
			  }
		  

			  //Create an array of file names
			  var imgs = JSON.parse(fileNames);

			  //Create imgs on page
			  //updateThumbList(imgs);


			};

			xhr.send(formData);
			updateUploadList(files);


		}
		
			function updateUploadList(imgs){


				//Create a thumbnail for each file
				for(var i = 0; i < imgs.length; i++){

					console.log("ImageName: "+imgs[i].name);

					var imgSpace = document.createElement('li');
					var imgName = document.createElement('p');

					// Create list item
					document.getElementById('imgList').appendChild(imgSpace);

					//Attach to imgSpace
					imgSpace.appendChild(imgName);

					imgName.innerHTML = imgs[i].name;
				}


			}

			function processImages(){
				$.post("processHandler/processImages",
			    { //Nothing to pass
			    },
			    function(data, status){
			        
			    	console.log("Move Files Finished");
			    	//console.log("Files: " + data);


			    	var msgSpace = document.getElementById('msgSpace');
			    	msgSpace.innerHTML = data + " Files Processed.";

			        //alert("Data: " + data + "\nStatus: " + status);
			    });


			}

			function downloadImages(){
				$.post("index.php/processHandler/downloadImages",
					{//Nothing to pass
				}, 
				function(data,status){
					


				});
			}

		</script>




	</head>

	<body>

		<h1> Process Page </h1>

		<h2> 

			<?

				if(isset($loggedIn) && $loggedIn ==1 ){
					echo "Welcome ".$name;
				}else{
					echo "Not Logged In";
				}


			?>

		</h2>

		<h3> 
			<?

			if(isset($error)){
				switch($error){
					case 1:
					echo 'Login to Set Preferences';
					break;

				}
			}

			?>

		</h3>

		<a href='<?echo base_url();?>index.php/processHandler/toLogin'> Login </a>

		<div id='uploadDiv'>
			<h2> Upload Form </h2>
			<form id="form" enctype="multipart/form-data">
				<input type="file" id="userfile" name="userfile" multiple>
				<input type="button" id="button" value="Upload" onclick="postMultiple()">

			</form>

			<p>



			</p>

			<div id='imgDisplayDiv'>


				<ul id='imgList' >



				</ul>


			</div>


			<br>
		</div>

		<p id='msgSpace'> </p>

		<input value="Process Files" type="button" onclick="processImages()"> <br><br>
		<input value="Download Files" type="button" onclick="downloadImages()"> <br>
		<a href='<?echo base_url();?>index.php/processHandler/toSettings'> Set Preferences </a> <br>
		<a href='<?echo base_url();?>index.php/processHandler/logout'> Logout </a>

		


		<!-- <input type="button" onclick="pressed()"> -->

		

	</body>

</html>
