<?

class loginHandler extends CI_Controller {


	public function __construct(){
		parent::__construct();

        $this->load->model('model_db');
	}

	public function index(){
		log_message('debug','routed -> loginHandler');
		$this->load->view('view_login');
	}


	//								   //
	//----------Validate Login---------//
	//								   //

	public function validateLogin(){
		log_message('debug','loginHandler: validateLogin');

		$email = $this->input->post('email');
		$password = $this->input->post('password');

		$this->load->library('form_validation');

		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
		$this->form_validation->set_rules('password', 'Password', 'required');

		//----Validate Form----//
		if($this->form_validation->run()){//Form Valid. Validate with db
			log_message('debug','loginHandler: loginForm VALID');
			$this->authenticateUser($email,$password);

		}else{//Form invalid. Return error message
			log_message('debug','loginHandler: loginForm INVALID');
			redirect('loginHandler');
		}

		
	}

	//								   //
	//--------Authenticate User--------//
	//								   //

	//Checks if email/password combination is valid
	public function authenticateUser($email,$password){
		log_message('debug','loginHandler: authenticateUser');

		$userData = $this->model_db->authenticateLogin($email,$password);

		$name = $userData->fName." ".$userData->lName;
		$userId = $userData->id;

		
		if(!is_null($userId)){//Proccede with login
			log_message('debug','userAuthenticated');

			$userData = array(
					'loggedIn'=>1,
					'name'=> $name,
					'email'=>$email,
					'id'=>$userId
			);



			$this->session->set_userdata($userData);

			redirect('processHandler');

		}else{//Return login error message.
			$data = array(
					'msg'=>null,
					'errorMsg'=>'Login Failed. Please Try Again.'
				);
			$this->load->view('loginHandler',$data);
		}

	}


	//								   //
	//------Validate Registration------//
	//								   //
	public function validateRegistration(){
		log_message('debug','formHandler: validateRegistration');

		$fName = $this->input->post('fName');
		$lName = $this->input->post('lName');
		$email = $this->input->post('email');
		$password = $this->input->post('password');

		$userData = array(
				'fName'=>$fName,
				'lName'=>$lName,
				'email'=>$email,
				'password'=>$password
			);

		$this->load->library('form_validation');

		$this->form_validation->set_rules('fName', 'First Name', 'required|trim');
		$this->form_validation->set_rules('lName', 'Last Name', 'required|trim');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[user.email]');
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('confirmPassword', 'Password Confirm', 'required|matches[password]');


		$this->form_validation->set_message('is_unique', 'Requested Email is Taken.');

		//----Validate Form----//
		if($this->form_validation->run()){//Form is valid
			log_message('debug','loginHandler: regForm VALID');

			$result = $this->model_db->createUser($userData);
			//Log in user. Open Home Page

			$this->authenticateUser($email,$password);

			//redirect('processHandler');
		}else{//Form Invalid. Reload registration page.
			log_message('debug','loginHandler: regForm INVALID');

			$this->load->view('view_register'); //Errors not showing
		}

		


	}

	public function toRegister(){
		log_message('debug','loginHandler: toRegister');
		$this->load->view('view_register');
	}

}