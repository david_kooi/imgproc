<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class processHandler extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
       {
            parent::__construct();



            $this->load->model('model_image');
            $this->load->model('model_db');

       }

	public function index()
	{
		$userData = $this->session->all_userdata();
		
		$data = array(
				'name'=>null,
				'email'=>null,
				'loggedIn'=>0,
				'id'=>null
		);

		//Pass user data if user if logged in.
		if(isset($userData['loggedIn']) && $userData['loggedIn'] == 1 ){
			log_message('debug','ProcessHandler: User logged in. ID: '.$userData['id']);

			$data['name'] = $userData['name'];
			$data['email'] = $userData['email'];
			$data['loggedIn'] = $userData['loggedIn'];
			$data['id'] = $userData['id'];
		}else{
			log_message('debug','ProcessHandler: User NOT logged in');
		}

		$this->load->view('view_processPage',$data);

	}

	public function toLogin(){
		redirect('loginHandler');
	}

	public function uploadImages(){
		log_message('debug','processHandler: uploadImages');


		$photos = $this->model_image->uploadImage();

		echo json_encode($photos);


	}

	public function processImages(){
		log_message('debug','processHandler: processImages');

		$numFiles = $this->model_image->processImages();

		echo $numFiles;

	}

	public function downloadImages(){
		$this->load->library('zip');

		$this->zip->read_dir(OUTPUTFOLDER);
		$this->zip->download('images.zip');

		//$zipFile = $this->model_image->downloadFiles();

		//$this->downloadTwo($zipFile);

	}

	public function download($file){
 	  log_message('debug','model_image: downloadFile');

	  header("Content-disposition: attachment; filename=$file");
	  header("Content-type: application/zip");
	  readfile($file);

}

	public function downloadTwo($zipFile){
		$this->load->library('zip');

		$this->zip->download();

	}
	public function toSettings(){
		log_message('debug','toSettings');

		$settings = null;

		//If user is logged in show all settings in settings page
		if(isset($this->session->all_userdata()['loggedIn']) && $this->session->all_userdata()['loggedIn'] == 1 ){
			log_message('debug','processHandler: toSettings: loggedIn');
			$id = $this->session->all_userdata()['id'];


			$settings = $this->model_db->getSettings($id);

			$data = array('settings'=>$settings);

			$this->load->view('view_settings',$data);
		}else{
			$data = array(
					'error'=>'1'
				);
			$this->load->view('view_processPage',$data);
		}
		
		
	}

	public function setSetting(){
		log_message('debug','processHandler: setSetting');

		$name = $this->input->post('name');
		$xSize = $this->input->post('xSize');
		$ySize = $this->input->post('ySize');

		$settingsData = array(
				'name'=>$name,
				'xSize'=>$xSize,
				'ySize'=>$ySize
			);
		$this->model_db->setPreference($settingsData);
		
	}

	public function logout(){
		$this->session->sess_destroy();
		redirect('processHandler');
	}


}

/* End of file process.php */
/* Location: ./application/controllers/process.php */