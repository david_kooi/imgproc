<?



class model_image extends CI_Model{
	 


	function uploadImage(){
		log_message('debug','model_image: uploadImage');

		//Stores file names
		$_PHOTOS = array();

		$numPhotos = count($_FILES['photos']['name']);
		log_message('debug','model_image: numPhotos: '.$numPhotos);

		/* Data format of $_FILES */

		// Process each photo
		for($i = 0; $i < $numPhotos; $i++){
		
			$photo = array('name'=>'',
						   'type'=>'',
						   'tmp_name'=>'',
						   'error'=>'',
						   'size'=>''
						   );

			/* 
				- Prepare photo for upload
				- Individual photos are extracted from $_FILES and a singular $photo is made
			*/      
			foreach ($photo as $key=>$value) {
		
				if(isset($_FILES['photos'][$key])){
					$photo[$key] = $_FILES['photos'][$key][$i];
				}
				

			}

			/*
				- Add $photo to $_FILES. 
				- do_upload() calls $_FILES['userfile']. 
			*/
			$_FILES['userfile'] = $photo;
		
			$fileName = $this->ciImageUpload();
			log_message('debug','model_image: fileName: '.$fileName);

			//Keep track of fileNames
			$_PHOTOS[$i] = $fileName;

			
		}	

		return $_PHOTOS;

	}

	function ciImageUpload(){

			if(!file_exists(TMP_FLD)){
				mkdir(TMP_FLD);
			}

			//CI_Upload preferences
			$config['upload_path'] = TMP_FLD;
			$config['allowed_types'] = 'gif|jpg|jpeg|png';
			$config['max_size']	= '200000';
			$config['max_width']  = '20000';
			$config['max_height']  = '20000';


			$this->load->library('upload', $config);

			if ( ! $this->upload->do_upload()){
				$error = array('error' => $this->upload->display_errors());

				return $error['error'];
			}
			else{ //Return image data to client side (TODO: Return data array)
				$data = $this->upload->data();

				return $data['file_name'];

			}
	}	

	function processImages(){
		// Get array of all source files
	$files = scandir(TMP_FLD);
	// Identify directories
	$source = TMP_FLD;
		
	$imgCount = 0;
	// Cycle through all source files
		foreach ($files as $file) {

		  //Ignore System Files
		  if (strpos($file, '.') == 0) continue;
		  $imgCount++;

		  // If we copied this successfully, mark it for deletion
			$image = new Imagick($source.$file);
			
			//getFormat, if not .jpg setformat to jpg
	
			// if($image->getImageFormat() != 'JPEG')
			// {
			// 	$image->setImageFormat("jpeg");
				
			// 	//if filename has .png suffix, strip png and add jpg
			// 	if(stripos($file, '.png') !== false)
			// 	{
			// 		$file = str_ireplace('.png', '.jpg', $file);
			// 	}
			// 	//remove .tif suffix and replace with .jpg
			// 	if(stripos($file, '.tif') !== false)
			// 	{
			// 		$file = str_ireplace('.tif', '.jpg', $file);
			// 	}
			// }
			
			//set height to 0 to keep aspect ratio
			
			 
			 $pref = array(
			 		'width'=>100,
			 		'quality'=>100,
			 		'destination'=>OUTPUTFOLDER
			 	);

			 //Export Sizes
			 $this->export($image,$file,$pref);

			 //Archive Original
			 $fileSource = $source.$file;
			 $fileDest = ARCHIVE.$file;

			 $this->archive($fileSource,$fileDest);

			 //Delete file after it is exported
			 unlink($fileSource);

		}
		return $imgCount;
	}

function export($image,$file,$pref){

	//set height to 0 to keep aspect ratio
	$height = 0;
	$width = $pref['width'];	
	$quality = $pref['quality'];	

	$destination_folder = $pref['destination'];

	$this->exportSize($image, $width, $height, $destination_folder, $file, $quality);
} 

function exportSize($image, $width, $height, $destination_folder, $file, $quality)
{
	if(!file_exists($destination_folder)){
		mkdir($destination_folder);
	}

	//Checks to make sure image is bigger than destination size
	if ($image->getImageWidth() > $width){
		$image->scaleImage($width,$height); // Providing 0 forces thumbnailImage to maintain aspect ratio
	}	
	$outputtype = $image->getFormat();
	
	//$destination_folder = 'output_folder/' . $destination_folder . '/';
	$image->setImageCompressionQuality($quality);
	
	$image->writeImage($destination_folder.$file);

}

function archive($source,$destination){
	if(!file_exists(ARCHIVE)){
		mkdir(ARCHIVE);
	}
	copy($source, $destination);
}

function downloadFiles(){
	$this->load->library('zip');
	$this->load->helper('download');

	$files = scandir(OUTPUTFOLDER);
	if(count($files) == 0 || !file_exists(OUTPUTFOLDER)){
		
	}

	foreach ($files as $file) {
		if (strpos($file, '.') == 0) continue;

		log_message('debug','fileName: '.OUTPUTFOLDER.$file);
				
		$this->zip->read_file(OUTPUTFOLDER.$file);
	}

	//$this->zip->add_data('test.txt','somestuff');
	
	$path = OUTPUTFOLDER.'archive.zip';

	$this->zip->archive($path);
	$zip = $this->zip->get_zip();
	return $zip;

	//return $path;
	

	//$zip = $this->zip->get_zip();
	//force_download('archive.zip',$zip);
	//$this->push_file($path,'archive.zip');
	
}

function downloadFile($file){
  log_message('debug','model_image: downloadFile');

  header("Content-disposition: attachment; filename=$file");
  header("Content-type: application/zip");
  readfile($file);

}

function push_file($path, $name)
{
  // make sure it's a file before doing anything!
	  if(is_file($path))
	  {
	  	log_message('debug','push_file: isFile');
	    // required for IE
	    if(ini_get('zlib.output_compression')) { ini_set('zlib.output_compression', 'Off'); }

	    // get the file mime type using the file extension
	    $this->load->helper('file');

	    $mime = get_mime_by_extension($path);

	    // Build the headers to push out the file properly.
	    header('Pragma: public');     // required
	    header('Expires: 0');         // no cache
	    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
	    header('Last-Modified: '.gmdate ('D, d M Y H:i:s', filemtime ($path)).' GMT');
	    header('Cache-Control: private',false);
	    header('Content-Type: '.$mime);  // Add the mime type from Code igniter.
	    header('Content-Disposition: attachment; filename="'.basename($name).'"');  // Add the file name
	    header('Content-Transfer-Encoding: binary');
	    header('Content-Length: '.filesize($path)); // provide file size
	    header('Connection: close');
	    readfile($path); // push it out
	    return;
	  }
}




	
}